//slider

jQuery(document).ready( function($) { 

        $('.slider').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: '<button type="button" class="slick-prev"><img src="assets/img/prev-slider.png"></img></button>',
            nextArrow: '<button type="button" class="slick-next"><img src="assets/img/next-slider.png"></img></button>',
            responsive: [
                {
                  breakpoint: 1440,
                  settings: {
                    slidesToShow: 2,
                  }
                },
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 2,
                  }
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 1,
                  }
                }]
        });
        $(window).scroll(function(){
            if ($(window).scrollTop() > 10) {
                $('.header').addClass('scroll');
            }
            else {
                $('.header').removeClass('scroll')
            }
        });
        const menuBtn = $('.menu-btn');
        const mobileMenu = $('nav.nav');
        let menuOpen = false;
        menuBtn.on('click', function() {
        if(!menuOpen) {
            menuBtn.addClass('open');
            mobileMenu.addClass('mobile');
            $('body').css('overflow', 'hidden');
            $('html').css('overflow', 'hidden');
            menuOpen = true;
        } else {
            menuBtn.removeClass('open');
            mobileMenu.removeClass('mobile');
            $('body').css('overflow', 'auto');
            $('html').css('overflow', 'auto');
            menuOpen = false;
        }
        });

} );


