"use strict";

var gulp = require('gulp'),
    path = require('path'),
    twig = require('gulp-twig'), 
    prefix = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),    
	  plumber = require('gulp-plumber'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync'),
    fs = require('fs');


var paths = {
  build: './build/',
  scss: './dist/scss/',
  js: './dist/js/'
};


gulp.task('twig', function () {
  return gulp.src(['./dist/templates/*.twig'])
	.pipe(plumber({
		handleError: function (err) {
			console.log(err);
			this.emit('end');
		}
	}))

  .pipe(
    twig().on('error', function (err) {
        process.stderr.write(err.message + '\n');
        this.emit('end');
    })
  )
	.pipe(gulp.dest(paths.build));
});


gulp.task('rebuild', ['twig'], function () {
  browserSync.reload();
});


gulp.task('browser-sync', ['sass', 'twig', 'js'], function () {
  browserSync({
    server: {
      baseDir: paths.build
    },
    notify: false,
    browser:"google chrome"
  });
});


gulp.task('sass', function () {
    return gulp.src(paths.scss + '/main.scss')
        .pipe(sourcemaps.init())
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(
            sass({
                includePaths: [paths.scss + '/'],
                outputStyle: 'compressed'
            }).on('error', function (err) {
                console.log(err.message);
                // sass.logError
                this.emit('end');
            })
        )
        .pipe(prefix(['last 15 versions','> 1%','ie 8','ie 7','iOS >= 9','Safari >= 9','Android >= 4.4','Opera >= 30'], {
            cascade: true
        }))	
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.build + '/assets/css/'));
  });


gulp.task('js', function(){
    return gulp.src(paths.js + 'script.js')
        .pipe(sourcemaps.init())
        .pipe(concat('script.min.js'))
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.build +'assets/js'));
});


gulp.task('watch', function () {
    gulp.watch(paths.js + 'script.js', ['js', browserSync.reload]);
    gulp.watch(paths.scss + '**/*.scss', ['sass', browserSync.reload]);
    gulp.watch(['dist/templates/**/*.twig'], {cwd:'./'}, ['rebuild']);
});

gulp.task('build', ['sass', 'twig']);


gulp.task('default', ['browser-sync', 'watch']);